# snake

In case you were wondering, I didn't invent Snake.

I'm writing my own little version of Snake here to help familiarise myself with
C and ncurses.

## General structure

I have a map and I have a game. The map is essentially the game plus a border,
but I separate them to isolate the game logic and prevent it from escaping into
the rest of the window I draw. It also just seems neater.

Apart from that, there's not much to document yet. This is not yet in a working
state. I'll post notes of how I build it when it's in a buildable condition.
