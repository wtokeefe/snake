/* Walter O'Keefe
 * 2015-07-22
 * Snake in ncurses */

#include <curses.h>
#include <string.h>

/* Direction <-> Keyboard input aliases */
#define UP 3
#define DOWN 2
#define RIGHT 5
#define LEFT 4

/* Map boundaries */
#define MAP_WIDTH 25  /* Left-to-right */
#define MAP_HEIGHT 25 /* Top-to-bottom */

/* Width of map border. 
 * Not fully implemented: draw_map doesn't handle drawing more than 1-char width
 * yet, but it is used to calculate GAME_* boundaries below.  */
#define MAP_BORDER_WIDTH 1

/* Characters used to represent map elements, which will not change */
#define MAP_CORNER '+' /* Char for corners of the map */
#define MAP_TOP '-'    /* Char for top and bottom sides */
#define MAP_SIDE '|'   /* Char for left and right sides */
#define MAP_BG ' '     /* Background char */

/* Game boundaries; the game will not be able to draw outside these */
#define GAME_WIDTH (MAP_WIDTH - MAP_BORDER_WIDTH)
#define GAME_HEIGHT (MAP_HEIGHT - MAP_BORDER_WIDTH)
/* Characters used to represent game elements, which may change */
#define GAME_FRUIT '*'
#define GAME_SNAKE 'o'

/* Error char for something to draw if it isn't set properly.
 * All char_to_draw declarations should default to this. */
#define ERROR_CHAR '\0'

/* map_coord for drawing background */
struct map_coord
{
    int x = 0, y = 0;
};
/* game_coord for drawing game, within borders of background */
struct game_coord
{
    int x = 0, y = 0;
};

/* Function declarations */
void draw_map (struct map_coord c);
void draw_game (struct map_coord c);


int main (void)
{
  keypad (initscr (), 1);
  noecho ();
  curs_set (0);
  draw_map ();
  
  char user_input = ERROR_CHAR;

  /* Controls */
  const char quit = 'q';
  const char start_game = 'g';
  const char move_left = LEFT;
  const char move_right = RIGHT;
  const char move_up = UP;
  const char move_down = DOWN;
  
  /* Insert Coin screen loop */
  while (user_input != start_game)
    {
      draw_insert_coin ();
    }
  /* Game loop */
  while (user_input != quit)
    {
      /* ...game... */
    }
}  

/* Draw game background (map) dynamically based on MAP_* defined above. */
void draw_map (void)
{
  /* Iterate over 2D coords */
  int x, y;
  for (y = 0; y < MAP_HEIGHT; y++)
    {
      for (x = 0; x < MAP_WIDTH; x++)
	{
	  /* Logic for drawing map borders */
	  int point_is_horz_edge = (x == 0 || x == MAP_WIDTH);
	  int point_is_vert_edge = (y == 0 || y == MAP_HEIGHT);
	  int point_is_corner = (point_is_horz_edge && point_is_vert_edge);
	  
	  
	  /* Use above logic to decide which char to draw at current coords */
	  char char_to_draw = ERROR_CHAR;
	  if (point_is_corner)
	    char_to_draw = MAP_CORNER;
	  else if (point_is_horz_edge)
	    char_to_draw = MAP_SIDE;
	  else if (point_is_vert_edge)
	    char_to_draw = MAP_TOP;
	  else /* Point is background */
	    char_to_draw = MAP_BG;

	  mvaddch (y, x, char_to_draw);
	}
    }
}

/* Draw interactive elements (snake, fruit) */
void draw_game (struct game_coord c)
{
  
}
